#include <iostream>
#include <string>

// Base animal
class Animal
{
public:
	virtual std::string Voice() const = 0;
	virtual const std::string& GetAnimalType() const = 0;
};

// Dog class
class Dog : public Animal
{
public:
	std::string Voice() const override { return "Woof"; }
	const std::string& GetAnimalType() const override { return animalType; }

private:
	const static std::string& animalType;
};
const std::string& Dog::animalType = "Dog";

// Cat class
class Cat : public Animal
{
public:
	std::string Voice() const override { return "Meow"; }
	const std::string& GetAnimalType() const override { return animalType; }

private:
	const static std::string& animalType;
};
const std::string& Cat::animalType = "Cat";

// Cow class
class Cow : public Animal
{
public:
	std::string Voice() const override { return "Muuu"; }
	const std::string& GetAnimalType() const override { return animalType; }

private:
	const static std::string& animalType;
};
const std::string& Cow::animalType = "Cow";

// Wolf class
class Wolf : public Animal
{
public:
	std::string Voice() const override { return "Rrrr"; }
	const std::string& GetAnimalType() const override { return animalType; }

private:
	const static std::string& animalType;
};
const std::string& Wolf::animalType = "Wolf";


int main()
{
	Dog dog1, dog2;
	Cat cat1, cat2;
	Cow cow1, cow2;
	Wolf wolf1, wolf2;
	
	Animal** AnimalList = new Animal * [] {&dog1, &cat1, &cow1, &wolf1, &cat2, &dog2, &wolf2, &cow2 };
	const int animalListSize = 8;

	for(int i = 0; i < animalListSize; ++i)
		std::cout << AnimalList[i]->GetAnimalType() << " says \"" << AnimalList[i]->Voice() << "\"\n";

	delete[] AnimalList;
	return 0;
}